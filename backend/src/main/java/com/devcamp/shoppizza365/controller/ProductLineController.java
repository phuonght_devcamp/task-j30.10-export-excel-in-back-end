package com.devcamp.shoppizza365.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.ProductLines;
import com.devcamp.shoppizza365.model.Products;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.repository.ProductLineRepository;
import com.devcamp.shoppizza365.repository.ProductRepository;

import java.util.*;

@RestController
public class ProductLineController {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ProductLineRepository productLineRepository;

	@PostMapping("/product_line/create")
	public ResponseEntity<Object> createProductLine(@RequestBody ProductLines cProductLine) {
		try {
			ProductLines newRole = new ProductLines();
			newRole.setDescription(cProductLine.getDescription());
			newRole.setProductLine(cProductLine.getProductLine());
			
						
			ProductLines savedRole = productLineRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/product_line/update/{id}")
	public ResponseEntity<Object> updateProdLine(@PathVariable("id") Long id, @RequestBody ProductLines cProductLine) {
		Optional<ProductLines> prodLineData = productLineRepository.findById(id);
		if (prodLineData.isPresent()) {
			ProductLines newProdLine = prodLineData.get();
			newProdLine.setDescription(cProductLine.getDescription());
						
			newProdLine.setProductLine(cProductLine.getProductLine());
			
			
			ProductLines savedProdLine = productLineRepository.save(newProdLine);
			return new ResponseEntity<>(savedProdLine, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/product_line/delete/{id}")
	public ResponseEntity<Object> deleteProdLnById(@PathVariable Long id) {
		try {
			productLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
	@GetMapping("/product_line/details/{id}")
	public ProductLines getProdLnById(@PathVariable Long id) {
		if (productLineRepository.findById(id).isPresent())
			return productLineRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/product_line/all")
	public List<ProductLines> getAllProductLine() {
		return productLineRepository.findAll();
	}

}
