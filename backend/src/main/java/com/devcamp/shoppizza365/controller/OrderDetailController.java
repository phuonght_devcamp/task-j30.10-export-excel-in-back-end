package com.devcamp.shoppizza365.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Order;
import com.devcamp.shoppizza365.model.OrderDetail;
import com.devcamp.shoppizza365.model.Products;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.repository.OrderDetailRepository;
import com.devcamp.shoppizza365.repository.OrderRepository;
import com.devcamp.shoppizza365.repository.ProductRepository;

import java.util.*;

@RestController
public class OrderDetailController {
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	@Autowired
	private ProductRepository productRepository;

	@GetMapping("/order_detail/all")
	public List<OrderDetail> getAllOrderDetail() {
		return orderDetailRepository.findAll();
	}
	
	
	@PostMapping("/order_detail/create/{idOrder}/{idProduct}")
	public ResponseEntity<Object> createOrder(@PathVariable("idOrder") Long id1,@PathVariable("idProduct") Long id2, @RequestBody OrderDetail cOrderDetail) {
		Optional<Order> orderData = orderRepository.findById(id1);
		Optional<Products> productData = productRepository.findById(id2);
		if (orderData.isPresent()&& productData.isPresent())
		{
			OrderDetail newRole = new OrderDetail();
			newRole.setPriceEach(cOrderDetail.getPriceEach());
			
			newRole.setQuantityOrder(cOrderDetail.getQuantityOrder());
			
			
			Order order = orderData.get();
			Products product = productData.get();
			newRole.setOrders(order);
			newRole.setProducts(product);
			
						
			OrderDetail savedRole = orderDetailRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/order_detail/update/{id}")
	public ResponseEntity<Object> updateOrderDetail(@PathVariable("id") Long id, @RequestBody OrderDetail cOrderDetail) {
		Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);
		if (orderDetailData.isPresent()) {
			OrderDetail newOrder = orderDetailData.get();
			
			
			newOrder.setPriceEach(cOrderDetail.getPriceEach());
			newOrder.setQuantityOrder(cOrderDetail.getQuantityOrder());
			
			
			OrderDetail savedOrderDetail = orderDetailRepository.save(newOrder);
			return new ResponseEntity<>(savedOrderDetail, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
////
	@DeleteMapping("/order_detail/delete/{id}")
	public ResponseEntity<Object> deleteorderDetailById(@PathVariable Long id) {
		try {
			orderDetailRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
	@GetMapping("/order_detail/details/{id}")
	public OrderDetail getOrderDetailById(@PathVariable Long id) {
		if (orderDetailRepository.findById(id).isPresent())
			return orderDetailRepository.findById(id).get();
		else
			return null;
	}

	

}
