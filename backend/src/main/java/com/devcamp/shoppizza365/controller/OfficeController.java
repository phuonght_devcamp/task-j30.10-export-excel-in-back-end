package com.devcamp.shoppizza365.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Office;
import com.devcamp.shoppizza365.repository.OfficeRepository;

import java.util.*;

@RestController
public class OfficeController {
	@Autowired
	private OfficeRepository officeRepository;

	@PostMapping("/office/create")
	public ResponseEntity<Object> createOffice(@RequestBody Office cOffice) {
		try {
			Office newRole = new Office();
			newRole.setAddress(cOffice.getAddress());
			newRole.setCity(cOffice.getCity());
			newRole.setCountry(cOffice.getCountry());
			newRole.setPhone(cOffice.getPhone());
			newRole.setState(cOffice.getState());
			newRole.setTerritory(cOffice.getTerritory());
			
			
						
			Office savedRole = officeRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/office/update/{id}")
	public ResponseEntity<Object> updateOffice(@PathVariable("id") Long id, @RequestBody Office cOffice) {
		Optional<Office> OfficeData = officeRepository.findById(id);
		if (OfficeData.isPresent()) {
			Office newOffice = OfficeData.get();
			
			newOffice.setAddress(cOffice.getAddress());
			newOffice.setCity(cOffice.getCity());
			newOffice.setCountry(cOffice.getCountry());
			newOffice.setPhone(cOffice.getPhone());
			newOffice.setState(cOffice.getState());
			newOffice.setTerritory(cOffice.getTerritory());
			 
	
			Office savedOffice = officeRepository.save(newOffice);
			return new ResponseEntity<>(savedOffice, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/office/delete/{id}")
	public ResponseEntity<Object> deleteOfficeById(@PathVariable Long id) {
		try {
			officeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
	@GetMapping("/office/details/{id}")
	public Office getOfficeById(@PathVariable Long id) {
		if (officeRepository.findById(id).isPresent())
			return officeRepository.findById(id).get();
		else
			return null;
	}


	@GetMapping("/office/all")
	public List<Office> getAllOffice() {
		return officeRepository.findAll();
	}
}
