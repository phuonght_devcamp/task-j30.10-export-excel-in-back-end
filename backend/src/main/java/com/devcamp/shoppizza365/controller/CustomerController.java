package com.devcamp.shoppizza365.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.service.ExcelExporter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@CrossOrigin
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepository;

	@PostMapping("/customer/create")
	public ResponseEntity<Object> createCustomer(@RequestBody Customer cCustomer) {
		try {
			Customer newRole = new Customer();
			newRole.setAddress(cCustomer.getAddress());
			newRole.setCity(cCustomer.getCity());
			newRole.setCountry(cCustomer.getCountry());
			newRole.setCreditLimit(cCustomer.getCreditLimit());
			newRole.setFirstName(cCustomer.getFirstName());
			newRole.setLastName(cCustomer.getLastName());
			newRole.setOrders(cCustomer.getOrders());
			newRole.setPayments(cCustomer.getPayments());
			newRole.setPhoneNumber(cCustomer.getPhoneNumber());
			newRole.setPostalCode(cCustomer.getPostalCode());
			newRole.setSalesRepEmployeeNumber(cCustomer.getSalesRepEmployeeNumber());
			newRole.setState(cCustomer.getState());
						
			Customer savedRole = customerRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/customer/update/{id}")
	public ResponseEntity<Object> updateCustomer(@PathVariable("id") Long id, @RequestBody Customer cCustomer) {
		Optional<Customer> customerData = customerRepository.findById(id);
		if (customerData.isPresent()) {
			Customer newCustomer = customerData.get();
			
			 newCustomer.setAddress(cCustomer.getAddress());
			 newCustomer.setCity(cCustomer.getCity());
			 newCustomer.setCountry(cCustomer.getCountry());
			 newCustomer.setCreditLimit(cCustomer.getCreditLimit());
			 newCustomer.setFirstName(cCustomer.getFirstName());
			 newCustomer.setLastName(cCustomer.getLastName());
			 newCustomer.setPhoneNumber(cCustomer.getPhoneNumber());
			 newCustomer.setPostalCode(cCustomer.getPostalCode());
			 newCustomer.setSalesRepEmployeeNumber(cCustomer.getSalesRepEmployeeNumber());
			 newCustomer.setState(cCustomer.getState());
			 newCustomer.setOrders(cCustomer.getOrders());
			 newCustomer.setPayments(cCustomer.getPayments());
			 
	
			Customer savedCustomer = customerRepository.save(newCustomer);
			return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/customer/delete/{id}")
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
		try {
			customerRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
	@GetMapping("/customer/details/{id}")
	public Customer getCustomerById(@PathVariable Long id) {
		if (customerRepository.findById(id).isPresent())
			return customerRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/customer/all")
	public List<Customer> getAllCustomer() {
		return customerRepository.findAll();
	}

	@GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=customers_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepository.findAll().forEach(customer::add);
		ExcelExporter excelExporter = new ExcelExporter(customer);
		excelExporter.export(response);
	}

}
