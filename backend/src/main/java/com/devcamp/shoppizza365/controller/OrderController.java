package com.devcamp.shoppizza365.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Order;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.repository.OrderRepository;
import com.devcamp.shoppizza365.service.ExcelExporter;
import com.devcamp.shoppizza365.service.ExcelExporterOrder;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class OrderController {
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping("/order/all")
	public List<Order> getAllOrder() {
		return orderRepository.findAll();
	}
	
	
	@PostMapping("/order/create/{id}")
	public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody Order cOrder) {
		Optional<Customer> customerData = customerRepository.findById(id);
		if (customerData.isPresent())
		{
			Order newRole = new Order();
			newRole.setComments(cOrder.getComments());
			
			newRole.setOrderDate(cOrder.getOrderDate());
			newRole.setOrderDetails(cOrder.getOrderDetails());
			newRole.setRequiredDate(cOrder.getRequiredDate());
			newRole.setShippedDate(cOrder.getShippedDate());
			newRole.setStatus(cOrder.getStatus());
			
			Customer _customer = customerData.get();
			newRole.setCustomer(_customer);
			
						
			Order savedRole = orderRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/order/update/{id}")
	public ResponseEntity<Object> updateorder(@PathVariable("id") Long id, @RequestBody Order cOrder) {
		Optional<Order> orderData = orderRepository.findById(id);
		if (orderData.isPresent()) {
			Order newOrder = orderData.get();
			newOrder.setComments(cOrder.getComments());
			
			newOrder.setOrderDate(cOrder.getOrderDate());
			newOrder.setOrderDetails(cOrder.getOrderDetails());
			newOrder.setRequiredDate(cOrder.getRequiredDate());
			newOrder.setShippedDate(cOrder.getShippedDate());
			newOrder.setStatus(cOrder.getStatus());
			
			Order savedOrder = orderRepository.save(newOrder);
			return new ResponseEntity<>(savedOrder, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/order/delete/{id}")
	public ResponseEntity<Object> deleteorderById(@PathVariable Long id) {
		try {
			orderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/order/details/{id}")
	public Order getOrderById(@PathVariable Long id) {
		if (orderRepository.findById(id).isPresent())
			return orderRepository.findById(id).get();
		else
			return null;
	}

	
	@GetMapping("/export/orders/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=orders_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Order> order = new ArrayList<Order>();
		orderRepository.findAll().forEach(order::add);
		ExcelExporterOrder excelExporter = new ExcelExporterOrder(order);
		excelExporter.export(response);
	}
}
