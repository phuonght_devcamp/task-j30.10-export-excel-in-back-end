package com.devcamp.shoppizza365.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
@Table(name="orders")
public class Order  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="comments")
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="order_date")
	private Date orderDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="required_date")
	private Date requiredDate;
//
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="shipped_date")
	private Date shippedDate;
	
	@Column(name="status")
	private String status;

	@OneToMany(mappedBy="orders",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<OrderDetail> orderDetails;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
    @JoinColumn(name="customer_id",nullable = false)
    private Customer customer;

public Order(long id, String comments, Date orderDate, Date requiredDate, Date shippedDate, String status,
		Customer customer) {
	super();
	this.id = id;
	this.comments = comments;
	this.orderDate = orderDate;
	this.requiredDate = requiredDate;
	this.shippedDate = shippedDate;
	this.status = status;
	this.customer = customer;
}

public Order() {
	super();
}



public String getComments() {
	return comments;
}

public void setComments(String comments) {
	this.comments = comments;
}

public Date getOrderDate() {
	return orderDate;
}

public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
}

public Date getRequiredDate() {
	return requiredDate;
}

public void setRequiredDate(Date requiredDate) {
	this.requiredDate = requiredDate;
}

public Date getShippedDate() {
	return shippedDate;
}

public void setShippedDate(Date shippedDate) {
	this.shippedDate = shippedDate;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public Customer getCustomer() {
	return customer;
}

public void setCustomer(Customer customer) {
	this.customer = customer;
}

public Order(long id, String comments, Date orderDate, Date requiredDate, Date shippedDate, String status,
		List<OrderDetail> orderDetails, Customer customer) {
	super();
	this.id = id;
	this.comments = comments;
	this.orderDate = orderDate;
	this.requiredDate = requiredDate;
	this.shippedDate = shippedDate;
	this.status = status;
	this.orderDetails = orderDetails;
	this.customer = customer;
}
//
public List<OrderDetail> getOrderDetails() {
	return orderDetails;
}

public void setOrderDetails(List<OrderDetail> orderDetails) {
	this.orderDetails = orderDetails;
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}





	
	

}