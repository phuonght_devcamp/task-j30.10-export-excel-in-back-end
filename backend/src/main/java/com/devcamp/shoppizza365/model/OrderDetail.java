package com.devcamp.shoppizza365.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

@Entity
@Table(name="order_details")
public class OrderDetail  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name="price_each")
	private BigDecimal priceEach;

	@Column(name="quantity_order")
	private int quantityOrder;
	
	@ManyToOne
	@JoinColumn(name = "order_id", nullable = false)
	@JsonIgnore
	private Order orders;
////	
	@ManyToOne
	@JoinColumn(name = "product_id", nullable = false)
	@JsonIgnore
	private Products products;
//
//	
//	
	public Products getProducts() {
		return products;
	}
	public void setProducts(Products products) {
		this.products = products;
	}
	public OrderDetail(int id, BigDecimal priceEach, int quantityOrder, Order orders, Products products) {
	super();
	this.id = id;
	this.priceEach = priceEach;
	this.quantityOrder = quantityOrder;
	this.orders = orders;
	this.products = products;
}
	public OrderDetail(int id, BigDecimal priceEach, int quantityOrder, Order orders) {
		super();
		this.id = id;
		this.priceEach = priceEach;
		this.quantityOrder = quantityOrder;
		this.orders = orders;
	}
public OrderDetail() {
	super();
}
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public BigDecimal getPriceEach() {
	return priceEach;
}
public void setPriceEach(BigDecimal priceEach) {
	this.priceEach = priceEach;
}
public int getQuantityOrder() {
	return quantityOrder;
}
public void setQuantityOrder(int quantityOrder) {
	this.quantityOrder = quantityOrder;
}
public Order getOrders() {
	return orders;
}
public void setOrders(Order orders) {
	this.orders = orders;
}

	
	
}