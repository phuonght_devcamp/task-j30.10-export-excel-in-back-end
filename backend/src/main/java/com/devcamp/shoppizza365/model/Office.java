package com.devcamp.shoppizza365.model;

import javax.persistence.*;

import org.springframework.lang.NonNull;


import java.util.List;
import java.util.Set;


@Entity
@Table(name="offices")
public class Office  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="city")
	private String city;
	@NonNull	
	@Column(name="phone")
	private String phone;
	
	@Column(name="address_line")
	private String address;
	

	@Column(name="state")
	private String state;

	@Column(name="country")
	private String country;
	
	
	
	
	@Column(name="territory")
	private String territory;




	public Office(int id, String city, String phone, String address, String state, String country, String territory) {
		super();
		this.id = id;
		this.city = city;
		this.phone = phone;
		this.address = address;
		this.state = state;
		this.country = country;
		this.territory = territory;
	}




	public Office() {
		super();
	}




	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public String getCity() {
		return city;
	}




	public void setCity(String city) {
		this.city = city;
	}




	public String getPhone() {
		return phone;
	}




	public void setPhone(String phone) {
		this.phone = phone;
	}




	public String getAddress() {
		return address;
	}




	public void setAddress(String address) {
		this.address = address;
	}




	public String getState() {
		return state;
	}




	public void setState(String state) {
		this.state = state;
	}




	public String getCountry() {
		return country;
	}




	public void setCountry(String country) {
		this.country = country;
	}




	public String getTerritory() {
		return territory;
	}




	public void setTerritory(String territory) {
		this.territory = territory;
	}

	
}