package com.devcamp.shoppizza365.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Order;



/**
 * @author HieuHN
 *
 */
public class ExcelExporterOrder {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	
	private List<Order> orders;
	

	/**
	 * Constructor khởi tạo server export danh sách order 
	 * @param orders
	 */
	public ExcelExporterOrder(List<Order> orders) {
		this.orders = orders;
		workbook = new XSSFWorkbook();
	}
	
	/**
	 * Tạo các ô cho excel file.
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	/**
	 * Khai báo cho sheet và các dòng đầu tiên
	 */
	private void writeHeaderLine() {
		sheet = workbook.createSheet("Orders");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "Order ID", style);
		createCell(row, 1, "Order Date", style);
		createCell(row, 2, "Require Date", style);
		createCell(row, 3, "Shipped Date", style);
		createCell(row, 4, "status", style);
		createCell(row, 5, "comments", style);

	}

	
	/**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (Order order : this.orders) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			
			
             DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
             
			String orderDate = dateFormat.format((order.getOrderDate()));
			String requiredDate = dateFormat.format((order.getRequiredDate()));
			DateFormat FULL_DATE_FORMATTER = new SimpleDateFormat( "yyyy-MM-dd");
			String shippedDateString = "0000-00-00 00:00:00";
	        if (order.getShippedDate() != null) {
	        	shippedDateString = FULL_DATE_FORMATTER.format((order.getShippedDate()));
	        }
	        
			

			createCell(row, columnCount++, String.valueOf(order.getId()), style);
			createCell(row, columnCount++, orderDate, style);
			createCell(row, columnCount++, requiredDate , style);
			createCell(row, columnCount++, shippedDateString, style);
			createCell(row, columnCount++, order.getStatus(), style);
			createCell(row, columnCount++, order.getComments(), style);

		}
	}

	/**
	 * xuất dữ liệu ra dạng file
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
