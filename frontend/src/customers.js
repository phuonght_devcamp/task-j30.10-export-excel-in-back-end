/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function(){
'use strict';

const gBASE_CUSTOMER_URL = "http://localhost:8080/customer/";

//onst gBASE_PHONGBAN_URL = "http://localhost:8080/phongban/";
// var gListPhongBan ={ 
//   phongBan:[

//     {id:"",
//     maPhongBan:"",
//     tenPhongBan:"",
//     gioiThieu:"",
//     nghiepVu:"",
//     }],
//     getPhongBanById: function(paramvPhongbanId){
//       var vIsFound = false;
//       var vI = 0;
//       var vPhongBanFound = {};
//       while (!vIsFound && vI < this.phongBan.length) {
//         if (this.phongBan[vI].id == paramvPhongbanId) {
//           vIsFound = true;
//           vPhongBanFound = this.phongBan[vI];
//         }
//         else vI++;
//       }
//       return vPhongBanFound;
//       },
//     }


var gListCustomer ={ 
  customers:[
 
              {
              }]
              // ,
              // getCustomerById: function(paramvNhanVienId){
              //   var vIsFound = false;
              //   var vI = 0;
              //   var vNhanVienFound = {};
              //   while (!vIsFound && vI < this.customer.length) {
              //     if (this.customer[vI].id == paramvNhanVienId) {
              //       vIsFound = true;
              //       vNhanVienFound = this.customer[vI];
              //     }
              //     else vI++;
              //   }
              //   return vNhanVienFound;
              //   },
              }
              var  gGenderValue = "";
      
      let gCustomerTable = $('#customer-table').DataTable({
        columns: [
    { data: 'id' },
    { data: 'address' },
    { data: 'city' },
    { data: 'state' },
    { data: 'country' },
    { data: 'creditLimit' },
    { data: 'firstName' },
    { data: 'lastName' },
    { data: 'phoneNumber' },
    { data: 'postalCode' },
    { data: 'salesRepEmployeeNumber' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i id="icon-update" class="fas fa-edit text-primary"></i>
      | <i id="icon-delete" class="fas fa-trash text-danger"></i>`,
    },
  ],
});

var gId="";
var gRowData = {};

// var gNhanVienObj = {
//   id:"",
//   maNhanVien:"",
//   hoTen:"",
//   chucVu:"",
//   gioiTinh:"",
//   tenPhongBan:"",
//   ngaySinh:"",
//   diaChi:"",
//   phone:"",
//   phongBan:"",
//   }
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// Export Excel Customer
// $("#btn-export-customer").on("click", function(){
//     onBtnExportCustomerDataClick();
//   })
//I: lấy Info nhân viên
// $("#btn-detail-customer").on("click", function(){
//   onBtnGetInfoNhanVienClick();
// })

// //C: Create nhân viên
// $("#btn-create-customer").on("click", function(){
//   onBtnCreateNhanVienClick();
// })
// //U: Update phòng ban
// $("#staff-table").on("click", "#icon-update", function() {
//   onIconEditNhanVienClick(this);
// });

// $("#update-customer").on("click", function(){
//   onBtnUpdateNhanVienClick();
// })

// //D: Delete order
// $("#staff-table").on("click", "#icon-delete", function() {
//   onIconDeleteNhanVienClick(this);
// });

// $("#modal-delete-confirm").on("click", "#btn-confirm-delete-order", function() {
//   onBtnConfirmClick(this);
// });
// //Delete all
// $("#delete-all-customer").on("click", function(){
//   onBtnDeleteAllNhanVienClick();
// })

// $("#modal-delete-all-confirm").on("click", "#btn-confirm-delete-all", function() {
//   onBtnConfirmDeleteAllClick(this);
// });
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
onPageLoading();

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//Hàm load trang
function onPageLoading(){
  "use strict";
  //callApiGetCustomerData();
  // callApiGetPhongBanData();
  // loadDataPhongBanToSelect();

}
// function onBtnGetInfoNhanVienClick(){
//   "use strict";
//   if ($("#input-id").val().trim()==""){
//     alert("Vui lòng nhập mã Id nhân viên")
//   }
//   else{
    
//     var vIdNhanVien = $("#input-id").val().trim();
//     callApiGetNhanVienInfoById(vIdNhanVien);
//   }
// }
// //Hàm gọi api get danh sách phòng ban
// function callApiGetNhanVienInfoById(vIdNhanVien){
//   "use strict";
//   $.ajax({
//     url: gBASE_CUSTOMER_URL + "details/" + vIdNhanVien,
//     type: "GET",
//     dataType: 'json',
//     success: function(responseObject){
//       //debugger;
//       gListCustomer.customers = responseObject;
//       console.log(gListCustomer)
//       loadDataToForm(gListCustomer.customers); 
//       //loadPhongBanToSelect();
//     },
//     error: function(error){
//       console.assert(error.responseText);
//       alert("Không tìm thấy nhân viên")
//     }
//   });
// }

// function onBtnDeleteAllNhanVienClick(){
//   "use strict";
//   $('#modal-delete-all-confirm').modal('show');
// }

// // hàm xử lý sự kiện goi api delete 
// function callApiDeleteAllNhanVien() {
//   //console.log(gOrderDb.orders)
//   "use strict";
//   $.ajax({
//     url: gBASE_CUSTOMER_URL + "delete_all" ,
//     type: "DELETE",
//     dataType: 'json',
//     success: function(){
//       callApiGetCustomerData();
//     },
//     error: function(error){
//       console.assert(error.responseText);
//     }
//   });
//  }
// // hàm xử lý sự kiện nút confirm delete all nhân viên
// function onBtnConfirmDeleteAllClick() {
//   callApiDeleteAllNhanVien();
//   $("#modal-delete-all-confirm").modal("hide");
// }

// // hàm xử lý sự kiện nút confirm delete 
// function onBtnConfirmClick() {
//   callApiDeleteNhanVien();
//   $("#modal-delete-confirm").modal("hide");
// }

// // hàm xử lý sự kiện goi api delete 
// function callApiDeleteNhanVien() {
//   //console.log(gOrderDb.orders)
//   "use strict";
//   $.ajax({
//     url: gBASE_CUSTOMER_URL + "delete/"+ gId ,
//     type: "DELETE",
//     dataType: 'json',
//     success: function(){
//       callApiGetCustomerData();
//     },
//     error: function(error){
//       console.assert(error.responseText);
//     }
//   });
//  }
//  // hàm xử lý sự kiện click icon delete
//  function onIconDeleteNhanVienClick(paramDeleteIcon) { 
//   "use strict"

//   //lưu thông tin id vào biến toàn cục
//   var vDataRow = getIdFromIcon(paramDeleteIcon);
//   gId = vDataRow.id;
 
//   $('#modal-delete-confirm').modal('show');
// }



// // Hàm bấm nút update Nhân viên
// function onBtnUpdateNhanVienClick(){ 
//   "use strict";
//   // 1.thu thập dữ liệu phòng ban
//   getDataNhanVien();
//   //2. validate dữ liệu input
//   var vIsDataValidated = validateDataNhanVien();
//   if(vIsDataValidated) {
//   //3. gọi api update danh sách phòng ban
//   callApiUpdateNhanVien(gNhanVienObj);

//   }}
//  // hàm xử lý sự kiện click icon edit
//  function onIconEditNhanVienClick(paramEditIcon) {
//   "use strict"
 
//   //lưu thông tin id phòng ban vào biến toàn cục
//   var vDataRow = getIdFromIcon(paramEditIcon);
//   gId = vDataRow.id;
  
//   var vNhanVienSelected = gListCustomer.getCustomerById(gId);
//   console.log(vNhanVienSelected)

//   loadDataToForm(vNhanVienSelected);
  
//   //callApiUpdatePhongBan();
// }

//  // hàm xử lý sự kiện goi api update phòng ban
//  function callApiUpdateNhanVien(paramgObjRequest) {
//   "use strict";
//   $.ajax({
//     url: gBASE_CUSTOMER_URL + "update/"+ gId,
//     type: "PUT",
//     dataType: 'json',
//     contentType:"application/json; charset=utf-8",
//     data: JSON.stringify(paramgObjRequest),
//     success: function(responseObject){
//       alert("Update Success!")
//       callApiGetCustomerData();
//     loadNhanVienToTable();
//     },
//     error: function(error){
//       console.assert(error.responseText);
//     }
//   });
//  }


// function loadDataToForm(paramvNhanvienSelected){ 
//   "use strict";
//   $("#input-maNhanvien").val(paramvNhanvienSelected.maNhanVien);
//   $("#input-fullname").val(paramvNhanvienSelected.hoTen);
//   $("#input-customer-subject").val(paramvNhanvienSelected.chucVu);
//   if(paramvNhanvienSelected.gioiTinh === "Nam"){ gGenderValue=1}
//   else{gGenderValue=2};
//   $("#select-gender ").val(gGenderValue).change();
//   $("#input-dob").val(paramvNhanvienSelected.ngaySinh);
//   $("#input-address").val(paramvNhanvienSelected.diaChi);
//   $("#input-phone").val(paramvNhanvienSelected.phone);
 
// }
// // hàm thu thập dữ liệu id 
// function getIdFromIcon(paramEditIcon) { 
//   "use strict"
//   var vTableRow = $(paramEditIcon).parents("tr")
//   var vRowData = gCustomerTable.row(vTableRow).data()
//   return vRowData;
//  }

// // Hàm bấm nút tạo đơn 
// function onBtnCreateNhanVienClick(){ 
//   "use strict";
//   // 1.thu thập dữ liệu phòng ban
//   getDataNhanVien();
//   //2. validate dữ liệu input
//   var vIsDataValidated = validateDataNhanVien();
//   if(vIsDataValidated) {
//   //3. gọi api lấy danh sách voucher
//   callApiCreateNhanVien();

//   }}

//   // hàm validate dữ liệu các trường input
// function validateDataNhanVien(){ 
//   "use strict";
//     if(gNhanVienObj.maNhanVien == "")  {
//       alert("Vui lòng nhập mã nhân viên"); 
//       return false;
//     }
//     if(gNhanVienObj.maNhanVien.length < 3)  {
//       alert("Mã nhân viên cần ít nhất 3 ký tự"); 
//       return false;
//     }
//     if(gNhanVienObj.hoTen == "")  {
//       alert("Vui lòng nhập họ tên"); 
//       return false;
//     }
//     if(gNhanVienObj.chucVu == "")  {
//       alert("Vui lòng nhập chức vụ"); 
//       return false;
//     }
//     if($("#select-gender").val()== 0)  {
//       alert("Vui lòng chọn giới tính"); 
//       return false;
//     }
//     if($("#select-phongban").val()== 0)  {
//       alert("Vui lòng chọn phòng ban"); 
//       return false;
      
//     }
//     if(gNhanVienObj.diaChi == "")  {
//       alert("Vui lòng nhập địa chỉ"); 
//       return false;
//     }
//     if(gNhanVienObj.phone == "")  {
//       alert("Vui lòng nhập số điện thoại"); 
//       return false;
//     }
//     if(gNhanVienObj.ngaySinh == "")  {
//       alert("Vui lòng nhập ngày sinh"); 
//       return false;
//     }
    
//     else {return true;}
// }

// function getDataNhanVien(){
//   "use strict";
//   gNhanVienObj.maNhanVien = $("#input-maNhanvien").val().trim().toUpperCase();
//   gNhanVienObj.hoTen = $("#input-fullname").val().trim();
//   gNhanVienObj.chucVu = $("#input-customer-subject").val().trim();
//   gNhanVienObj.gioiTinh = $("#select-gender :selected").text().trim();
//  gGenderValue =  $("#select-gender :selected").val()
//   gNhanVienObj.ngaySinh = $("#input-dob").val();
//   gNhanVienObj.diaChi = $("#input-address").val().trim();
//   gNhanVienObj.phone = $("#input-phone").val().trim();
//   gNhanVienObj.tenPhongBan = $("#select-phongban :selected").text();
//   console.log(gNhanVienObj)
// }
// function callApiCreateNhanVien(){
//   "use strict"

// //console.log(vObjectRequest);
//   $.ajax({
//     url: gBASE_CUSTOMER_URL + "create",
//     dataType: "json",
//     type: "POST",
//     contentType: "application/json",
//     async: false,  // tham số async nhận giá trị fasle
//     data: JSON.stringify(gNhanVienObj),
//     success: function(responseObject){
//       //debugger;
     
//       console.log(responseObject);
//      alert("Create Success!")
//      callApiGetCustomerData();
//         },
//     error: function(error){
//      alert("Trùng Mã nhân viên")
//     }
//   })
  
// };

// //Hàm gọi api get danh sách phòng ban
// function callApiGetPhongBanData(){
//   "use strict";
//   $.ajax({
//     url: gBASE_PHONGBAN_URL + "all",
//     type: "GET",
//     dataType: 'json',
//     success: function(responseObject){
//       //debugger;
//       gListPhongBan.phongBan = responseObject;
//       console.log(gListPhongBan)
     
//       loadDataPhongBanToSelect(gListPhongBan.phongBan);
//     },
//     error: function(error){
//       console.assert(error.responseText);
//     }
//   });
// }
// function loadDataPhongBanToSelect(){
//   "use strict"
//   var vPhongbanSelect = $("#select-phongban");
//     for (var bI = 0; bI < gListPhongBan.phongBan.length; bI++) {
//       var vOption = $("<option/>", {
//         value: gListPhongBan.phongBan[bI].maPhongBan,
//         text: gListPhongBan.phongBan[bI].tenPhongBan
//       }).appendTo(vPhongbanSelect);
//     }
// }
// Hàm bấm nút export data Customer
// function onBtnExportCustomerDataClick(){
//   "use strict";
//   $.ajax({
//         url: "http://localhost:8080/export/customers/excel",
//         type: "GET",
//         dataType: 'json',
//         success: function(responseObject){
//          alert("Export success!");
//         },
//         error: function(error){
//           console.assert(error.responseText);
//         }
//       });
// }
//Hàm gọi api get danh sách phòng ban
function callApiGetCustomerData(){
  "use strict";
  $.ajax({
    url: gBASE_CUSTOMER_URL + "all",
    type: "GET",
    dataType: 'json',
    
    success: function(responseObject){
      //debugger;
      gListCustomer.customers = responseObject;
      console.log(gListCustomer)
      loadCustomerToTable(gListCustomer.customers); 
      //loadPhongBanToSelect();
    },
    error: function(error){
      console.assert(error.responseText);
    }
  });
}
function loadCustomerToTable(paramNhanVien) {
  gCustomerTable.clear();
  gCustomerTable.rows.add(paramNhanVien);
  gCustomerTable.draw();
}


})